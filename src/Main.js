import React, { Component } from 'react';

import './customStyles/foundation.min.css';
import './customStyles/custom.css';
import Routes from './routes';
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import "bootstrap/dist/css/bootstrap.css";
import { Redirect } from 'react-router-dom';
import { Button } from 'react-bootstrap';


class App extends Component {

  constructor() {
    super();
    this.state = {
      appName: "teste-luby-front",
      home: false,
      redirectToReferrer: false
    }
  
  }

 

  render() {

    if (this.state.redirectToReferrer) {
      return (<Redirect to={'/login'} />)
    }
    return (
      <div className="off-canvas-wrapper">
        <div className="off-canvas-wrapper-inner" data-off-canvas-wrapper>
          <div className="off-canvas-content" data-off-canvas-content>
            <Navbar bg="dark" variant="dark">
              <Navbar.Brand href="/">Luby</Navbar.Brand>
              <Nav className="mr-auto">
                <Nav.Link href={"/create-aluno"}>Criar Aluno</Nav.Link>
                <Nav.Link href={"/list-aluno"}>Listar Alunos</Nav.Link>

              </Nav>
            </Navbar>
            <Routes name={this.state.appName} />
            <hr />

          </div>
        </div>
      </div>
    );
  }
}

export default App;