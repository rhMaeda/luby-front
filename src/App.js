import React from "react";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import "bootstrap/dist/css/bootstrap.css";
import "./App.css";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import EditAluno from "./components/edit-aluno.component";
import AlunoList from "./components/list-aluno.component";
import CreateAluno from "./components/create-aluno.component";

function App() {
  return (
  <Router>
    <div className="App">
      <Navbar bg="dark" variant="dark">
        <Navbar.Brand href="#home">Luby</Navbar.Brand>
        <Nav className="mr-auto">
          <Nav.Link href={"/create-aluno"}>Criar Aluno</Nav.Link>
          <Nav.Link href={"/list-aluno"}>Listar Alunos</Nav.Link>
        </Nav>
      </Navbar>

      <Container style={{marginTop: '10em'}}>
        <Row>
          <Col md={12}>
            <div className="wrapper">
              <Switch>
                <Route exact path='/' component={CreateAluno} />
                <Route path="/create-aluno" component={CreateAluno} />
                <Route path="/edit-aluno/:id" component={EditAluno} />
                <Route path="/list-aluno" component={AlunoList} />
              </Switch>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  </Router>);
}

export default App;