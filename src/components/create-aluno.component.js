import React, { Component } from "react";
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import axios from 'axios'
import AlunoList from './list-aluno.component';
import {Redirect} from 'react-router-dom';
import Swal from 'sweetalert2';


export default class CreateAluno extends Component {
  constructor(props) {
    super(props)

    // Setting up functions
    this.onChangeAlunoNome = this.onChangeAlunoNome.bind(this);
    this.onChangeAlunoRg = this.onChangeAlunoRg.bind(this);
    this.onChangeAlunoTurma = this.onChangeAlunoTurma.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    // Setting up state
    this.state = {
      nome: '',
      rg: '',
      turma: '',
      redirectToReferrer: false
    }
    console.log(this.state.redirectToReferrer, "teste")
  }

  onChangeAlunoNome(e) {
    this.setState({ nome: e.target.value })
  }

  onChangeAlunoRg(e) {
    this.setState({ rg: e.target.value })
  }

  onChangeAlunoTurma(e) {
    this.setState({ turma: e.target.value })
  }

  onSubmit(e) {
    e.preventDefault()
    const aluno = {
      nome: this.state.nome,
      rg: this.state.rg,
      turma: this.state.turma,
      redirectToReferrer: false
    };
    axios.post('http://localhost:8000/api/alunos/', aluno)
      .then((res) => {
        if (res) {
          let timerInterval
          Swal.fire({
            title: 'Aluno sendo incluido',
            html: 'A tela fechara em <b></b> ms.',
            timer: 2000,
            timerProgressBar: true,
            didOpen: () => {
              Swal.showLoading()
              timerInterval = setInterval(() => {
                const content = Swal.getContent()
                if (content) {
                  const b = content.querySelector('b')
                  if (b) {
                    b.textContent = Swal.getTimerLeft()
                  }
                }
              }, 100)
            },
            willClose: () => {
              clearInterval(timerInterval)
            }
          }).then((result) => {
            if (result.dismiss === Swal.DismissReason.timer) {
              window.location.reload(false);
            }
          })
        }
      });

    this.setState({ nome: '', rg: '', turma: '' })
  }

  componentWillMount() {

    if (sessionStorage.getItem("userData")) {
      console.log("entrou aquui")
    } else {
      this.setState({ redirectToReferrer: true });
    }


  }
  render() {
    if (this.state.redirectToReferrer) {
      return (<Redirect to={'/login'} />)
    }

    return (<div className="form-wrapper">
      <div>
        <Form onSubmit={this.onSubmit}>
          <Container>
          <Row>

            <Col>
              <Form.Group controlId="nome">
                <Form.Label>Nome</Form.Label>
                <Form.Control type="text" value={this.state.nome} onChange={this.onChangeAlunoNome} />
              </Form.Group>

            </Col>

            <Col>
              <Form.Group controlId="rg">
                <Form.Label>RG</Form.Label>
                <Form.Control type="text" value={this.state.rg} onChange={this.onChangeAlunoRg} />
              </Form.Group>
            </Col>

            <Col>
            <Form.Group controlId="turma">
              <Form.Label>Turma</Form.Label>
              <Form.Control type="text" value={this.state.turma} onChange={this.onChangeAlunoTurma} />
            </Form.Group>
            </Col>

          </Row>
          <Button variant="primary" size="lg" type="submit">
            Add Aluno
          </Button>
          </Container>



          
        </Form>
        <br></br>
        <br></br>

        <AlunoList> </AlunoList>
      </div>
    </div>
    );

  }
}