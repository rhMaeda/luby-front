import React, { Component } from "react";
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button';
import axios from 'axios';
import { Container, Row, Col } from "react-bootstrap";
import Swal from 'sweetalert2';

export default class EditAluno extends Component {

  constructor(props) {
    super(props)

    this.onChangeAlunoNome = this.onChangeAlunoNome.bind(this);
    this.onChangeAlunoRg = this.onChangeAlunoRg.bind(this);
    this.onChangeAlunoTurma = this.onChangeAlunoTurma.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    // State
    this.state = {
      nome: '',
      rg: '',
      turma: ''
    }
  }

  componentDidMount() {
    console.log("ëdit")
    axios.get('http://localhost:8000/api/alunos/' + this.props.match.params.id)
      .then(res => {
        this.setState({
          nome: res.data.nome,
          rg: res.data.rg,
          turma: res.data.turma
        });
      })
      .catch((error) => {
        console.log(error);
      })
  }

  onChangeAlunoNome(e) {
    this.setState({ nome: e.target.value })
  }

  onChangeAlunoRg(e) {
    this.setState({ rg: e.target.value })
  }

  onChangeAlunoTurma(e) {
    this.setState({ turma: e.target.value })
  }

  onSubmit(e) {
    e.preventDefault()

    const AlunoObject = {
      nome: this.state.nome,
      rg: this.state.rg,
      turma: this.state.turma
    };
    console.log(AlunoObject)
    axios.put('http://localhost:8000/api/alunos/' + this.props.match.params.id, AlunoObject)
      .then((res) => {
        if (res) {
          let timerInterval
          Swal.fire({
            title: 'Aluno sendo incluido',
            html: 'A tela fechara em <b></b> ms.',
            timer: 2000,
            timerProgressBar: true,
            didOpen: () => {
              Swal.showLoading()
              timerInterval = setInterval(() => {
                const content = Swal.getContent()
                if (content) {
                  const b = content.querySelector('b')
                  if (b) {
                    b.textContent = Swal.getTimerLeft()
                  }
                }
              }, 100)
            },
            willClose: () => {
              clearInterval(timerInterval)
            }
          }).then((result) => {
            if (result.dismiss === Swal.DismissReason.timer) {
              window.location.reload(false);
            }
          })
        }
      }).catch((error) => {
        console.log(error)
      })
    //retorna pra listagem dos alunos
    this.props.history.push('/list-aluno')
  }


  render() {
    return (
    <Container>
    <Row>
      <Col>
      <div className="form-wrapper">
      <Form onSubmit={this.onSubmit} >
        <Form.Group controlId="nome">
          <Form.Label>Nome</Form.Label>
          <Form.Control type="text" value={this.state.nome} onChange={this.onChangeAlunoNome} />
        </Form.Group>

        <Form.Group controlId="rg">
          <Form.Label>RG</Form.Label>
          <Form.Control type="text" value={this.state.rg} onChange={this.onChangeAlunoRg} />
        </Form.Group>

        <Form.Group controlId="turma">
          <Form.Label>Turma</Form.Label>
          <Form.Control type="text" value={this.state.turma} onChange={this.onChangeAlunoTurma} />
        </Form.Group>

        <Button variant="danger" size="lg" type="submit">
                Atualizar aluno
        </Button>
      </Form>
    </div>
      </Col>
    </Row>
    </Container>
    );
  }
}