import React, { Component } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';

import './Welcome.css';

class Welcome extends Component {

  constructor(props) {
    super(props)
    this.state = {
      redirectToReferrer: false
    };

    console.log(this.state.redirectToReferrer, "list aladfasduno")
  }

  UNSAFE_componentWillMount() {

    if (sessionStorage.getItem("userData")) {
      this.setState({ redirectToReferrer: true });
    }


  }
  render() {
    if (this.state.redirectToReferrer) {
      return (<Redirect to={'/home'} />)
    }
    return (
      <Container>
        <Row>
          <Col>
            <div className="row " id="Body">
              <div className="md columns">
                <h2 id="welcomeText">Teste luby tela inicial</h2>
                <a href="/login" className="button mybtn" >Login</a>
                <a href="/signup" className="button mybtn success" >Cadastrar</a>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
      
    );
  }
}

export default Welcome;