import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Button from 'react-bootstrap/Button';
import Swal from 'sweetalert2';
import axios from 'axios';

export default class AlunoTableRow extends Component {
    constructor(props) {
        super(props);
        this.deleteAluno = this.deleteAluno.bind(this);
    }

    deleteAluno() {
        axios.delete('http://localhost:8000/api/alunos/' + this.props.obj.id)
            .then((res) => {
                if (res) {
                    let timerInterval
                    Swal.fire({
                        title: 'Aluno sendo excluido',
                        html: 'A tela fechara em <b></b> ms.',
                        timer: 2000,
                        timerProgressBar: true,
                        didOpen: () => {
                            Swal.showLoading()
                            timerInterval = setInterval(() => {
                                const content = Swal.getContent()
                                if (content) {
                                    const b = content.querySelector('b')
                                    if (b) {
                                        b.textContent = Swal.getTimerLeft()
                                    }
                                }
                            }, 100)
                        },
                        willClose: () => {
                            clearInterval(timerInterval)
                        }
                    }).then((result) => {
                        if (result.dismiss === Swal.DismissReason.timer) {
                            window.location.reload(false);
                        }
                    })
                }

            }).catch((error) => {
                console.log(error)
            })
    }
    render() {
        return (
            <tr>
                <td>{this.props.obj.nome}</td>
                <td>{this.props.obj.rg}</td>
                <td>{this.props.obj.turma}</td>
                <td>
                    <Link className="edit-link" to={"/edit-aluno/" + this.props.obj.id}>
                        <Button size="sm" variant="info">Edit</Button>
                    </Link>
                    <Button onClick={this.deleteAluno} size="sm" variant="danger">Excluir</Button>
                </td>
            </tr>
        );
    }
}