import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import './Home.css';
import CreateAluno from "../create-aluno.component";

//import {PostData} from '../../services/PostData';
//import '../../customStyles/react-confirm-alert.css';
//import ReactConfirmAlert, { confirmAlert } from 'react-confirm-alert'; 


class Home extends Component {


  constructor(props) {
    super(props);

    this.state = {
      data: [],
      redirectToReferrer: false,
      name: '',
    };
      
  }

  componentWillMount() {

    if (sessionStorage.getItem("userData")) {
      console.log("entrou aquui")
    } else {
      this.setState({ redirectToReferrer: true });
    }


  }


  
  render() {
    if (this.state.redirectToReferrer) {
      return (<Redirect to={'/login'} />)
    }

    return (
      
      <CreateAluno />
   
     
    );
  }
}

export default Home;