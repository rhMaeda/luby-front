import React, { Component } from "react";
import axios from 'axios';
import Table from 'react-bootstrap/Table';
import AlunoTableRow from './AlunoTableRow';
import {Redirect} from 'react-router-dom';
import { Container, Row, Col } from "react-bootstrap";



export default class AlunoList extends Component {

  constructor(props) {
    super(props)
    this.state = {
      alunos: [],
      redirectToReferrer: false
    };

    console.log(this.state.redirectToReferrer, "list aluno")
  }

  componentWillMount() {

    if (sessionStorage.getItem("userData")) {
      console.log("entrou aquui")
    } else {
      this.setState({ redirectToReferrer: true });
    }


  }
  componentDidMount() {
    axios.get('http://localhost:8000/api/alunos/')
      .then(res => {
        this.setState({
          alunos: res.data
        });
      })
      .catch((error) => {
        console.log(error);
      })
  }

  DataTable() {
    return this.state.alunos.map((res, i) => {
      return <AlunoTableRow obj={res} key={i} />;
    });
  }


  render() {
    if (this.state.redirectToReferrer) {
      return (<Redirect to={'/login'} />)
    }
    return (
      <Container>
        <Row>
          <Col md>
            <div className="table-wrapper">
              <Table striped bordered hover>
                <thead>
                  <tr>
                    <th>Nome</th>
                    <th>Rg</th>
                    <th>Turma</th>
                    <th>Botao</th>
                  </tr>
                </thead>
                <tbody>
                  {this.DataTable()}
                </tbody>
              </Table>
            </div>
          </Col>
        </Row>

      </Container>
    );
  }
}