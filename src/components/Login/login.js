import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import {PostData} from '../../services/PostData';
import './login.css';

class Login extends Component {

  constructor(){
    super();
   
    this.state = {
     email: '',
     senha: '',
     redirectToReferrer: false
    };

    this.login = this.login.bind(this);
    this.onChange = this.onChange.bind(this);
    console.log(this.state)
  }

  

  login() {

    if(this.state.email && this.state.senha){
      console.log(this.state)
      PostData('login',this.state).then((result) => {
       let responseJson = result;
       if(responseJson.userData){         
         sessionStorage.setItem('userData',JSON.stringify(responseJson));
         this.setState({redirectToReferrer: true});
       }
       
      });
    }
    
   }

  onChange(e){
    this.setState({[e.target.name]:e.target.value});
   }

  
  

  render() {

     if (this.state.redirectToReferrer) {
      return (<Redirect to={'/home'}/>)
    }
   
    if(sessionStorage.getItem('userData')){
      return (<Redirect to={'/home'}/>)
    }

     return (
      <div className="row" id="Body">
        <div className="medium-5 columns left custom">
        <h4>Login</h4>
        <label>Email</label>
        <input type="text" name="email" placeholder="Email" onChange={this.onChange}/>
        <label>Senha</label>
        <input type="password" name="senha"  placeholder="Senha" onChange={this.onChange}/>
        <input type="submit" className="button success" value="Login" onClick={this.login}/>
        <a href="/signup">Registro</a>
        </div>
      </div>
    );
  }
}

export default Login;







