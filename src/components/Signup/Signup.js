import React, {Component} from 'react';
import {PostData} from '../../services/PostData';
import {Redirect} from 'react-router-dom';
import './Signup.css';
import Swal from 'sweetalert2';

class Signup extends Component {

  constructor(props){
    super(props);
   
    this.state = {
     senha: '',
     email: '',
     redirectToReferrer: false
    };

    this.signup = this.signup.bind(this);
    this.onChange = this.onChange.bind(this);

  }
 

  signup() {

    if(this.state.senha && this.state.email){
    PostData('signup',this.state).then((result) => {
      if (result) {
        let timerInterval
        Swal.fire({
          title: 'Cadastro concluido',
          html: 'A tela sera redirecionada em <b></b> ms.',
          timer: 4000,
          timerProgressBar: true,
          didOpen: () => {
            Swal.showLoading()
            timerInterval = setInterval(() => {
              const content = Swal.getContent()
              if (content) {
                const b = content.querySelector('b')
                if (b) {
                  b.textContent = Swal.getTimerLeft()
                }
              }
            }, 100)
          },
          willClose: () => {
            clearInterval(timerInterval)
          }
        }).then((result) => {
          if (result.dismiss === Swal.DismissReason.timer) {
            this.setState({redirectToReferrer: true});
          }
        })
      }
      let responseJson = result;
      if(responseJson.userData){         
        sessionStorage.setItem('userData',JSON.stringify(responseJson));
        this.setState({redirectToReferrer: true});
      }
      
     });
    }
  }

 onChange(e){
   this.setState({[e.target.name]:e.target.value});
  }

  render() {
    if (this.state.redirectToReferrer || sessionStorage.getItem('userData')) {
      return (<Redirect to={'/home'}/>)
    }
   
  

    return (
      
      <div className="row " id="Body">
        <div className="medium-5 columns left custom">
        <h4>Cadastro</h4>
        <label>Email</label>
        <input type="text" name="email"  placeholder="Email" onChange={this.onChange}/>
        <label>Senha</label>
        <input type="password" name="senha"  placeholder="Password" onChange={this.onChange}/>
        
        <input type="submit" className="button" value="Cadastrar" onClick={this.signup}/>
        <a href="/login">Login</a>
        </div>
        
      </div>
    );
  }
}

export default Signup;