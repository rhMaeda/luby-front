import React from 'react';
import {BrowserRouter,  Route,  Switch} from 'react-router-dom';

import Welcome from '././components/Welcome/Welcome';
import Home from '././components/Home/Home.js';
import Login from '././components/Login/login.js';
import Signup from '././components/Signup/Signup';
import NotFound from '././components/NotFound/NotFound';
import EditAluno from './components/edit-aluno.component';
import AlunoList from './components/list-aluno.component';
import CreateAluno from './components/create-aluno.component';


const Routes = () => (
  <BrowserRouter >
      <Switch>
          <Route exact path="/" component={Welcome}/>
          <Route path="/home" component={Home}/>
          <Route path="/login" component={Login}/>
          <Route path="/Signup" component={Signup}/>
          <Route path="/create-aluno" component={CreateAluno}/>
          <Route path="/list-aluno" component={AlunoList}/>
          <Route path="/edit-aluno/:id" component={EditAluno}/>
          <Route path="*" component={NotFound}/>
      </Switch>
  </BrowserRouter>
);

export default Routes;