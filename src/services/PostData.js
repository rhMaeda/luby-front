export function PostData(type, userData) {
    let BaseURL = 'http://127.0.0.1:8000/api/';

    return new Promise((resolve, reject) =>{
    
        fetch(BaseURL+type, {
          headers : { 
            'Content-Type': 'application/json',
            'Accept': 'application/json'
           },
            method: 'POST',
            body: JSON.stringify(userData)
          })
          .then((response) => response.json())
          .then((res) => {
            console.log(res)
            resolve(res);
          })
          .catch((error) => {
            reject(error);
          });

  
      });
}